 package com.aps.viewapsapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.data.models.User
import com.aps.viewapsapp.databinding.DrawerBinding
import com.aps.viewapsapp.databinding.MainActivityBinding
import com.aps.viewapsapp.events.details.EventActivity
import com.aps.viewapsapp.events.layouts.DashboardFragment
import com.aps.viewapsapp.user.CurrentUser
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.toast
import com.aps.viewapsapp.workers.NotificationWorker
import com.google.android.material.snackbar.Snackbar
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

 class MainActivity: AppCompatActivity() {

    private val menu by lazy {
        SlidingRootNavBuilder(this).withMenuLayout(R.layout.drawer).inject()
    }

    private val menuBinding by lazy {
        DrawerBinding.bind(menu.layout.findViewById(R.id.root))
    }

    private val binding by lazy { MainActivityBinding.inflate(layoutInflater) }

    private val dashboardFragment by lazy { DashboardFragment.newInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
                .add(R.id.fcvMainPlaceholder, dashboardFragment)
                .commit()

        binding.bottomAppBar.setNavigationOnClickListener {
            with(menu) { if (isMenuClosed) openMenu() else closeMenu() }
        }

        if (CurrentUser.privileges.create)
            binding.fabAdd.setOnClickListener {
                startActivityForResult(Intent(this, EventActivity::class.java).apply {
                    putExtra(EventActivity.Mode.ARG, EventActivity.Mode.Create)
                }, EventActivity.REQUEST_CODE)
            }
        else
            binding.fabAdd.hide()

        lifecycleScope.launch(Dispatchers.Main) {
            val user = withContext(Dispatchers.IO) { CurrentUser.fetch() }

            with (menuBinding) {
                txvUserName.txvText.text = "${user?.name ?: ""} ${user?.lastName ?: ""}"
                txvUserEmail.text = user?.email
                txvUserType.text = user?.userType?.let {
                    if (it != User.UserType.OTHER) getString(it.id) else null
                } ?: getString(CurrentUser.privileges.type)


                root.setOnClickListener {
                    menu.closeMenu()
                }

                btnLogOut.setOnClickListener {
                    CurrentUser.logOut(this@MainActivity)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EventActivity.REQUEST_CODE) {
            val event = data?.getSerializableExtra(Event.ARG) as? Event
            when (resultCode) {
                EventActivity.EVENT_CREATED -> {
                    if (event != null)
                        dashboardFragment.addEvent(event)
                }
                EventActivity.EVENT_EDITED -> {
                    if (event != null)
                        dashboardFragment.updateEvent(event)
                }
                EventActivity.EVENT_DELETED -> {
                    if (event != null)
                        removeOnSnackBarDismissal(event)
                }
            }

            lifecycleScope.launch {
                NotificationWorker.seekNewestEvent()?.let { newestEvent ->
                    NotificationWorker.updateCachedTimestamp(newestEvent.timestamp)
                }
            }
        }
    }

    private fun removeOnSnackBarDismissal(event: Event) {
        dashboardFragment.removeEvent(event)
        Snackbar.make(binding.root, R.string.deleted, Snackbar.LENGTH_LONG).apply {
            anchorView = binding.fabAdd
            setAction(R.string.undo) { dashboardFragment.addEvent(event) }
            addCallback(object: Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, eventAction: Int) {
                    super.onDismissed(transientBottomBar, eventAction)
                    if (eventAction != DISMISS_EVENT_ACTION)
                        lifecycleScope.launch(Dispatchers.Main) {
                            val result = withContext(Dispatchers.IO) { event.delete() }
                            if (result)
                                toast(R.string.deleteSuccessful)
                            else {
                                toast(R.string.deleteFailed)
                                dashboardFragment.addEvent(event)
                            }
                        }
                }
            })
            show()
        }
    }
}
