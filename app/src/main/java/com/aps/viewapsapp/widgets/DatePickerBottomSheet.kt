package com.aps.viewapsapp.widgets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.aps.viewapsapp.R
import com.aps.viewapsapp.databinding.DateTimePickerBinding
import com.aps.viewapsapp.utils.show
import com.aps.viewapsapp.utils.toDateTime
import com.aps.viewapsapp.utils.toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.paulrybitskyi.valuepicker.ValuePickerView
import com.paulrybitskyi.valuepicker.model.PickerItem
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class DatePickerBottomSheet(private val sfm: FragmentManager,
                            private val listener: OnDatePickedListener,
                            private val isTimeSelector: Boolean): BottomSheetDialogFragment() {

    interface OnDatePickedListener {

        fun onDateSelected(date: LocalDate)

        fun onDateTimeSelected(dateTime: LocalDateTime)

    }

    companion object {
        const val DEFAULT_HOUR = 12
        const val DEFAULT_MINUTE = 0
    }

    private val binding by lazy { DateTimePickerBinding.inflate(layoutInflater) }

    private val initialDate = LocalDate.now().atTime(DEFAULT_HOUR, DEFAULT_MINUTE)

    private val selectedDateTime: MutableLiveData<LocalDateTime> = MutableLiveData(initialDate)

    private val today: LocalDateTime
        get() = selectedDateTime.value?.let {
            LocalDateTime.now().withHour(it.hour).withMinute(it.minute).withSecond(0)
        } ?: initialDate

    private val dFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
    private val dtFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

    override fun onCreateView(i: LayoutInflater, c: ViewGroup?, b: Bundle?): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with (binding) {

            txvToday.setOnClickListener {
                selectedDateTime.value = today
            }

            txvTomorrow.setOnClickListener {
                selectedDateTime.value = today.plusDays(1)
            }

            txvPickADate.setOnClickListener {
                MaterialDatePicker.Builder.datePicker().also { builder ->
                    builder.setTitleText(R.string.pickDate)
                    builder.setCalendarConstraints(CalendarConstraints.Builder().setStart(System.currentTimeMillis()).build())
                    builder.build().apply {
                        addOnPositiveButtonClickListener { epochMillis ->
                            val pickedDate = epochMillis.toDateTime()
                            if (pickedDate.isBefore(LocalDateTime.now().plusHours(6)))
                                toast(R.string.onlyFutureDates)
                            else
                                selectedDateTime.value = selectedDateTime.value?.let {
                                    pickedDate.withHour(it.hour).withMinute(it.minute)
                                } ?: pickedDate
                        }

                        show(sfm, null)
                    }
                }
            }

            if (isTimeSelector) {
                rnlTimePicker.show()
                vpvHour.items = (0 until 24).map { PickerItem(it, String.format("%02d", it)) }
                vpvHour.setSelectedItem(PickerItem(DEFAULT_HOUR, DEFAULT_HOUR.toString()))
                vpvHour.onItemSelectedListener = ValuePickerView.OnItemSelectedListener { item ->
                    selectedDateTime.value?.let {
                        selectedDateTime.value = it.withHour(item.id)
                    }
                }

                vpvMinute.items = (0 until 60).map { PickerItem(it, String.format("%02d", it)) }
                vpvMinute.setSelectedItem(PickerItem(DEFAULT_MINUTE, DEFAULT_MINUTE.toString()))
                vpvMinute.onItemSelectedListener = ValuePickerView.OnItemSelectedListener { item ->
                    selectedDateTime.value?.let {
                        selectedDateTime.value = it.withMinute(item.id)
                    }
                }
            }

            btnConfirm.setOnClickListener {
                if (selectedDateTime.value?.isBefore(LocalDateTime.now().plusHours(6)) == true)
                    toast(R.string.onlyFutureDates)
                else {
                    if (isTimeSelector)
                        listener.onDateTimeSelected(selectedDateTime.value!!)
                    else
                        listener.onDateSelected(selectedDateTime.value!!.toLocalDate())
                    dismiss()
                }
            }

            btnCancel.setOnClickListener {
                dismiss()
            }
        }

        selectedDateTime.observe(viewLifecycleOwner) {
            binding.txvSelectedDate.txvText.text =
                    it.format(if (isTimeSelector) dtFormatter else dFormatter)
        }
    }

}