package com.aps.viewapsapp.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.aps.viewapsapp.R
import com.aps.viewapsapp.databinding.TextViewBinding

class TextView(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs) {

    private val binding: TextViewBinding =
            TextViewBinding.bind(inflate(context, R.layout.text_view, this))

    val txvHint: TextView = binding.txvHint
    val txvText: TextView = binding.txvText

    init {
        with (context.theme.obtainStyledAttributes(attrs, R.styleable.TextView, 0, 0)) {
            val drawableTint = getColor(R.styleable.TextView_drawableColor, context.getColor(R.color.black))
            val drawableStart = getDrawable(R.styleable.TextView_drawableStart)?.apply { setTint(drawableTint) }
            val drawableTop = getDrawable(R.styleable.TextView_drawableTop)?.apply { setTint(drawableTint) }
            val drawableEnd = getDrawable(R.styleable.TextView_drawableEnd)?.apply { setTint(drawableTint) }
            val drawableBottom = getDrawable(R.styleable.TextView_drawableBottom)?.apply { setTint(drawableTint) }

            txvText.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableStart, drawableTop, drawableEnd, drawableBottom)
            txvText.setTextAppearance(getResourceId(R.styleable.TextView_textAppearance, R.style.TextAppearance_ViewAPS_Body1))
            txvText.setTextColor(getColor(R.styleable.TextView_textColor, context.getColor(R.color.black)))
            txvText.text = getString(R.styleable.TextView_text)
            txvText.gravity = getInt(R.styleable.TextView_android_gravity, Gravity.CENTER_VERTICAL)
            txvText.textAlignment = getInt(R.styleable.TextView_android_textAlignment, TextView.TEXT_ALIGNMENT_TEXT_START)

            txvHint.textAlignment = getInt(R.styleable.TextView_android_textAlignment, TextView.TEXT_ALIGNMENT_TEXT_START)
            txvHint.setTextColor(getColor(R.styleable.TextView_hintColor, context.getColor(R.color.grey_700)))
            txvHint.text = getString(R.styleable.TextView_hint)

            recycle()
        }
    }

}