package com.aps.viewapsapp.widgets

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.aps.viewapsapp.R
import com.aps.viewapsapp.databinding.GroupBinding
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.getAttr


class Group(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs) {

    private val binding = GroupBinding.bind(inflate(context, R.layout.group, this))

    val group = binding.rnlGroup
    val title = binding.txvTitle

    init {
        with (context.theme.obtainStyledAttributes(attrs, R.styleable.Group, 0, 0)) {
            val colorBackground = context.getAttr(R.attr.colorBackground)
            val colorOnBackground = context.getAttr(R.attr.colorOnBackground)
            val background = getColor(R.styleable.Group_android_background, colorBackground)

            group.backgroundColor = background
            title.setBackgroundColor(background)

            title.text = getString(R.styleable.Group_android_title) ?: "Could not find title"
            title.setTextColor(getColor(R.styleable.Group_android_titleTextColor, colorOnBackground))

            recycle()
        }
    }


    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams?) {
        if (child.id == R.id.rnlGroup || child.id == R.id.txvTitle)
            super.addView(child, index, params)
        else
            group.addView(child, index, params)
    }

}