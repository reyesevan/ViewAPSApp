package com.aps.viewapsapp.widgets

import android.content.Context
import android.os.Parcelable
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.util.SparseArray
import androidx.constraintlayout.widget.ConstraintLayout
import com.aps.viewapsapp.R
import com.aps.viewapsapp.databinding.EditTextBinding
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class EditText(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs) {

    private val binding: EditTextBinding =
            EditTextBinding.bind(inflate(context, R.layout.edit_text, this))

    val layout: TextInputLayout = binding.inputLayout
    val editText: TextInputEditText = binding.editText

    var maxLength: Int
        get() = layout.counterMaxLength
        set(value) {
            layout.isCounterEnabled = value > 0
            layout.counterMaxLength = value
            editText.filters = if (value <= 0) emptyArray() else arrayOf(InputFilter.LengthFilter(value))
        }

    var text: String?
        get() = editText.text.toString()
        set(value) {
            editText.setText(value)
        }

    init {
        with (context.theme.obtainStyledAttributes(attrs, R.styleable.EditText, 0, 0)) {
            val drawableTint = getColor(R.styleable.EditText_drawableColor, context.getColor(R.color.black))
            val drawableStart = getDrawable(R.styleable.EditText_drawableStart)?.apply { setTint(drawableTint) }
            val drawableTop = getDrawable(R.styleable.EditText_drawableTop)?.apply { setTint(drawableTint) }
            val drawableEnd = getDrawable(R.styleable.EditText_drawableEnd)?.apply { setTint(drawableTint) }
            val drawableBottom = getDrawable(R.styleable.EditText_drawableBottom)?.apply { setTint(drawableTint) }

            editText.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableStart, drawableTop, drawableEnd, drawableBottom)
            editText.setTextAppearance(getResourceId(R.styleable.EditText_textAppearance, R.style.TextAppearance_ViewAPS_Body1))
            editText.setTextColor(getColor(R.styleable.EditText_textColor, context.getColor(R.color.black)))
            editText.setText(getString(R.styleable.EditText_text))
            editText.inputType = getInt(R.styleable.EditText_android_inputType, InputType.TYPE_TEXT_FLAG_MULTI_LINE)

            layout.hint = getString(R.styleable.EditText_hint)

            recycle()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        editText.isEnabled = enabled
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable?>?) {
        // Makes sure that the state of the child views in the side
        // spinner are not saved since we handle the state in the
        // onSaveInstanceState.
        super.dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable?>?) {
        // Makes sure that the state of the child views in the side
        // spinner are not restored since we handle the state in the
        // onSaveInstanceState.
        super.dispatchThawSelfOnly(container)
    }
}