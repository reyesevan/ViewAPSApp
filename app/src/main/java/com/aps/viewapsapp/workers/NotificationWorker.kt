package com.aps.viewapsapp.workers

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.aps.viewapsapp.App
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.data.models.Events
import com.aps.viewapsapp.events.details.EventActivity
import com.aps.viewapsapp.utils.*
import com.donkey.dolly.Dolly
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime

class NotificationWorker(context: Context, params: WorkerParameters): CoroutineWorker(context, params) {

    companion object {

        const val NAME = "ViewAPS.NotificationWorker"

        private val notificationId: Int
            get() = (System.currentTimeMillis() % Int.MAX_VALUE).toInt()

        fun updateCachedTimestamp(timestamp: LocalDateTime) {
            Dolly.getInstance(App.context).encrypt(R.string.timestampKey, timestamp.toDBString())
        }

        suspend fun seekNewestEvent(): Event? {
            val cached: String = Dolly.getInstance(App.context).decryptString(R.string.timestampKey) ?: ""
            val latestEvent: Event? = withContext(Dispatchers.IO) {
                Events.query.run {
                    orderBy(Event.Column.Timestamp.field, false)
                    limitTo(1)
                    find()
                }.firstOrNull()
            }

            return if (latestEvent?.timestamp?.toDBString() != cached) latestEvent else null
        }

    }

    override suspend fun doWork(): Result {
        val newestEvent = seekNewestEvent()

        if (newestEvent != null) {
            updateCachedTimestamp(newestEvent.timestamp)

            val eventIntent = Intent(applicationContext, EventActivity::class.java).apply {
                putExtra(EventActivity.Mode.ARG, EventActivity.Mode.Visualize)
                putExtra(EventActivity.REQUIRE_AUTHENTICATION, true)
                putExtra(Event.ARG, newestEvent)
            }

            val pendingIntent = TaskStackBuilder.create(applicationContext).run {
                addNextIntentWithParentStack(eventIntent)
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }

            val notification = NotificationCompat.Builder(applicationContext, CHANNEL_ID).run {
                setSmallIcon(R.drawable.img_s)
                setContentTitle(applicationContext.getString(R.string.newEventNotifTitle))
                setContentText(newestEvent.description)
                setContentIntent(pendingIntent)
                build()
            }

            NotificationManagerCompat.from(applicationContext).notify(notificationId, notification)
        }

        return Result.success()
    }
}