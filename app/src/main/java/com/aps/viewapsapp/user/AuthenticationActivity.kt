package com.aps.viewapsapp.user

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import com.donkey.dolly.Dolly
import com.donkey.dolly.Type
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.rbddevs.splashy.Splashy
import com.aps.viewapsapp.MainActivity
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.User
import com.aps.viewapsapp.databinding.LogInActivityBinding
import com.aps.viewapsapp.utils.*
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AuthenticationActivity: AppCompatActivity(), OnAuthenticationListener {

    private val binding by lazy { LogInActivityBinding.inflate(layoutInflater) }
    private val viewModel: AuthenticationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            CurrentUser.logIn(this@AuthenticationActivity)
        }

        Splashy(this@AuthenticationActivity).showAPSSplash()
        Splashy.onComplete(object: Splashy.OnComplete {
            override fun onComplete() {
                setContentView(binding.root)
                lifecycleScope.launch {
                    delay(100)
                    binding.txtUserEmail.showKeyboard()
                }
            }
        })

        viewModel.authenticationListener = this

        with (binding) {
            txtUserEmail.clearErrorOnTextChanged(tilUserEmail)
            txtUserPassword.clearErrorOnTextChanged(tilUserPassword)

            btnAuthenticate.setOnClickListener {
                val email = txtUserEmail.text.toString()
                val password = txtUserPassword.text.toString()

                val emailField = TextField(email, tilUserEmail, mapOf(
                    getString(R.string.nonEmpty) to { (it as String).nonEmpty() },
                    getString(R.string.invalidEmail) to { (it as String).validEmail() }
                ))

                val passwordField = TextField(password, tilUserPassword, mapOf(
                    getString(R.string.nonEmpty) to { (it as String).nonEmpty() }
                ))

                if (Validation.validateInputLayoutFields(emailField, passwordField))
                    lifecycleScope.launch {
                        viewModel.signIn(email, password)
                    }
            }
        }

    }

    override fun onPause() {
        super.onPause()
        binding.txtUserEmail.hideKeyboard()
    }

    override fun onSignInFailed(error: OnAuthenticationListener.Error, cachedCredentials: Boolean) {
        if (cachedCredentials) {
            toast(R.string.sessionExpired)
            CurrentUser.logOut(this)
        } else
            when (error) {
                OnAuthenticationListener.Error.USER_NOT_FOUND ->
                    binding.tilUserEmail.error = getString(R.string.noUserFound)
                OnAuthenticationListener.Error.INCORRECT_PASSWORD ->
                    binding.tilUserPassword.error = getString(R.string.incorrectPassword)
                else -> Unit
            }
    }

    override fun onSignInSuccessful(user: User, cachedCredentials: Boolean) {
        if (!cachedCredentials)
            user.encryptAndPersistCredentials(binding.chbKeepSignedIn.isChecked)

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun Splashy.showAPSSplash() = apply {
//        setAnimation(Splashy.Animation.SLIDE_IN_LEFT_BOTTOM)
        setAnimation(Splashy.Animation.SLIDE_LEFT_ENTER)
//        setBackgroundResource(R.drawable.img_background)
//        setBackgroundColor(R.color.indigo_700)
        setBackgroundColor(R.color.comet)
        setLogo(R.drawable.img_aps_logo)
        setTitle(R.string.app_name_title)
        setSubTitle(R.string.app_name_subtitle)
        setTitleColor(R.color.grey_200)
        setSubTitleColor(R.color.grey_400)
        setFullScreen(true)
        setClickToHide(true)
        show()
    }

}