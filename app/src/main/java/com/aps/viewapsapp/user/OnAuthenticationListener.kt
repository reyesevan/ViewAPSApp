package com.aps.viewapsapp.user

import com.aps.viewapsapp.data.models.User

interface OnAuthenticationListener {

    enum class Error { USER_NOT_FOUND, INCORRECT_PASSWORD, CREDENTIALS_NOT_FOUND }

    fun onSignInFailed(error: Error, cachedCredentials: Boolean = false)

    fun onSignInSuccessful(user: User, cachedCredentials: Boolean = false)

}