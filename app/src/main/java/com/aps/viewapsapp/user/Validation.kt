package com.aps.viewapsapp.user

import com.google.android.material.textfield.TextInputLayout
import java.time.LocalDate
import java.time.LocalDateTime

typealias Validator = (Any) -> Boolean

interface Field {
    val value: Any
    val layout: TextInputLayout
    val validations: Map<String, Validator>
}

data class TextField (
    override val value: String,
    override val layout: TextInputLayout,
    override val validations: Map<String, Validator>
): Field

data class DateField (
    override val value: LocalDate,
    override val layout: TextInputLayout,
    override val validations: Map<String, Validator>
): Field

data class DateTimeField (
        override val value: LocalDateTime,
        override val layout: TextInputLayout,
        override val validations: Map<String, Validator>
): Field

object Validation {

    fun validateInputLayoutFields(vararg fields: Field): Boolean {
        fields.forEach { field ->
            field.validations.forEach { (errorMessage, validate) ->
                if (validate(field.value).not()) {
                    field.layout.error = errorMessage
                    return false
                }
            }
        }
        return true
    }

}