package com.aps.viewapsapp.user

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.aps.viewapsapp.App
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.User
import com.aps.viewapsapp.data.models.Users
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.decryptInt
import com.aps.viewapsapp.utils.decryptString
import com.aps.viewapsapp.utils.encrypt
import com.donkey.dolly.Dolly
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object CurrentUser {

    var email: String?
        get() = Dolly.getInstance(App.context).decryptString(R.string.emailKey)
        set(value) = Dolly.getInstance(App.context).encrypt(R.string.emailKey, value)

    var password: String?
        get() = Dolly.getInstance(App.context).decryptString(R.string.passwordKey)
        set(value) = Dolly.getInstance(App.context).encrypt(R.string.passwordKey, value)

    var privilegesLevel: Int?
        get() = Dolly.getInstance(App.context).decryptInt(R.string.privilegesKey)
        set(value) = Dolly.getInstance(App.context).encrypt(R.string.privilegesKey, value)

    val privileges: User.Privileges
        get() = privilegesLevel?.let { currentLevel ->
            User.Privileges.values().firstOrNull { privileges ->
                privileges.level == currentLevel
            }
        } ?: User.Privileges.Dummy

    suspend fun fetch(): User? = email?.let {
        Users.findUserByEmail(it)
    }

    suspend fun logIn(listener: OnAuthenticationListener) {
        val cachedEmail = email
        val cachedPassword = password
        if (cachedEmail != null && cachedPassword != null) {
            val currentUser = withContext(Dispatchers.IO) { Users.findUserByEmail(cachedEmail) }
            if (currentUser?.credentialsMatch(cachedPassword) == true)
                listener.onSignInSuccessful(currentUser, true)
            else
                listener.onSignInFailed(OnAuthenticationListener.Error.INCORRECT_PASSWORD, true)
        } else
            listener.onSignInFailed(OnAuthenticationListener.Error.CREDENTIALS_NOT_FOUND, false)
    }

    fun logOut(activity: Activity) {
        email = null
        password = null
        privilegesLevel = null
        activity.startActivity(Intent(activity, AuthenticationActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        })
        activity.finish()
    }
}