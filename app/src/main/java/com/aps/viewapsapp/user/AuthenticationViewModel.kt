package com.aps.viewapsapp.user

import androidx.lifecycle.ViewModel
import com.aps.viewapsapp.data.models.Users
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AuthenticationViewModel: ViewModel() {

    var authenticationListener: OnAuthenticationListener? = null

    suspend fun signIn(email: String, password: String) {
        val user = withContext(Dispatchers.IO) { Users.findUserByEmail(email) }
        if (user == null)
            authenticationListener?.onSignInFailed(OnAuthenticationListener.Error.USER_NOT_FOUND)
        else {
            if (user.credentialsMatch(password))
                authenticationListener?.onSignInSuccessful(user)
            else
                authenticationListener?.onSignInFailed(OnAuthenticationListener.Error.INCORRECT_PASSWORD)
        }
    }

}