package com.aps.viewapsapp

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.work.*
import com.aps.viewapsapp.utils.CHANNEL_DESCRIPTION
import com.aps.viewapsapp.utils.CHANNEL_ID
import com.aps.viewapsapp.utils.CHANNEL_NAME
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.workers.NotificationWorker
import com.donkey.dolly.Dolly
import java.util.concurrent.TimeUnit

class App: Application() {

    companion object {
        lateinit var context: Context
        var inputMethodManager: InputMethodManager? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

            NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT).also {
                it.description = CHANNEL_DESCRIPTION
                notificationManager?.createNotificationChannel(it)
            }
        }

        val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val periodicWork = PeriodicWorkRequest
                .Builder(NotificationWorker::class.java, 15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()

//        WorkManager.getInstance(this).cancelUniqueWork(NotificationWorker.NAME)
        WorkManager.getInstance(this).enqueueUniquePeriodicWork(NotificationWorker.NAME,
                ExistingPeriodicWorkPolicy.KEEP, periodicWork)
    }

}