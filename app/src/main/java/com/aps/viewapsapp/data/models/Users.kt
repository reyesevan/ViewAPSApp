package com.aps.viewapsapp.data.models

import com.BoardiesITSolutions.AndroidMySQLConnector.ResultSet
import com.aps.viewapsapp.data.database.SQLTable

object Users: SQLTable<User>( "usuario") {

    override fun ResultSet?.toObjectOrNull(): User? =
        if (this != null && numRows >= 1) User(nextRow) else null

    override fun ResultSet?.toList(): List<User> = this?.let { result ->
        (0 until result.numRows).map { User(result.nextRow) }
    } ?: emptyList()

    suspend fun findUserByEmail(email: String): User? =
        select(table, null, User.EMAIL to email).toObjectOrNull()

}
