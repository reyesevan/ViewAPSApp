package com.aps.viewapsapp.data.models

import androidx.annotation.StringRes
import com.BoardiesITSolutions.AndroidMySQLConnector.MySQLRow
import com.aps.viewapsapp.App
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.database.SQLRow
import com.aps.viewapsapp.events.details.EventItem
import com.aps.viewapsapp.utils.*
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.reflect.KMutableProperty1

class Event(): SQLRow(), Serializable {

    companion object {
        const val ARG = "Event"
        val applicants = listOf( "Angelica Sanchez", "Guillermo Rodriguez",
                "Alberto Valdivia Vargas", "Paola Osornio", "Rubi Pelaez", "Pamela Hernández",
                "Erick Hernandez", "Marco Antonio Hernandez", "Jose Ricardo Hernández", "Katia Lopez",
                "Sandra Castelan", "Mariza González", "Ana Rojas", "Yarhabi Sandoval", "Korina Rubio",
                "Oscar Tendero", "Nydia Martinez", "Liz Samperio", "Enrique Prida", "Mercadotecnia",
                "Diana Calderon", "Alejandro Escobar")
    }

    constructor(row: MySQLRow): this() {
        setValuesFromRow(row)
    }

    enum class Edition(override val dbValue: String, override val id: Int? = null): APSEnum<Edition> {
        Puebla("Puebla"),
        Tlaxcala("Tlaxcala"),
        Hidalgo("Hidalgo"),
        Chiapas("Chiapas")
    }

    enum class Product(@StringRes override val id: Int, override val dbValue: String): APSEnum<Product> {
        Synthesis(R.string.synthesis, "Síntesis"),
        Faces(R.string.faces, "Rostros"),
        Both(R.string.both, "Ambos");

        override fun toString(): String = App.context.getString(id)
    }

    enum class EditionType(@StringRes override val id: Int, override val dbValue: String): APSEnum<EditionType> {
        Regular(R.string.regular, " Regular"),
        Couple(R.string.couple, "Especial Novios"),
        RealState(R.string.realState, " Especial Inmobiliarias"),
        Entrepreneurs(R.string.entrepreneurs, "Especial Empresarios"),
        Schools(R.string.schools, "Especial Escuelas"),
        Children(R.string.children, "Especial Niños"),
        Mothers(R.string.mothers, "Especial Mamás"),
        Fathers(R.string.fathers, "Especial Papás"),
        Tourism(R.string.tourism, "Especial Turismo"),
        Sports(R.string.sports, "Especial Deporte"),
        Gastronomy(R.string.gastronomy, "Especial Gastronomía"),
        Fashion(R.string.fashion, "Especial Moda"),
        Health(R.string.health, "Edición Salud"),
        Churches(R.string.churches, "Especial Iglesias"),
        Collection(R.string.collection, "Colección");

        override fun toString(): String = App.context.getString(id)
    }

    enum class EventType(@StringRes override val id: Int, override val dbValue: String): APSEnum<EventType> {
        Paid(R.string.paid, "Pagado"),
        Courtesy(R.string.courtesy, "Cortesía"),
        APHCourtesy(R.string.aphCourtesy, "Cortesía APH"),
        Patronage(R.string.patronage, "Patrocinio"),
        Exchange(R.string.exchange, "Intercambio"),
        PoliticalConvention(R.string.politicalConvention, "Convenio Político"),
        Social(R.string.social, "Social"),
        EnriqueCourtesy(R.string.enriqueCourtesy, "Cortesía Enrique"),
        SocialCommission(R.string.socialCommission, "Encargo Social"),
        CommercialCommission(R.string.commercialCommission, "Encargo Comercial"),
        EasyCode(R.string.easycode, "Easycode"),
        Shooting(R.string.shooting, "Shooting");

        override fun toString(): String = App.context.getString(id)
    }

    enum class Column(val field: String,
                      val length: Int = Int.MAX_VALUE,
                      val property: KMutableProperty1<Event, out Any>) {
        ID("Id", property = Event::id),
        Edition("Edicion", property = Event::edition),
        Product("Producto", property = Event::product),
        EditionType("Tipo_Edicion", property = Event::editionType),
        EventType("Tipo_Evento", property = Event::eventType),
        Description("Desc_Evento", 50, Event::description),
        PictureType("Tipo_Foto", property = Event::pictureType),
        Pages("Cant_plana", 300, Event::pages),
        Applicant("Solicitante", property = Event::applicant),
        PublicationDate("Fecha_Publi", property = Event::publicationDate),
        CoverageDate("Fecha_Cober", property = Event::coverageDate),
        EventDuration("Tiempo_Evento", 50, Event::eventDuration),
        Place("Lugar", 200, Event::place),
        ContactName("Nombre_Contacto", 200, Event::contactName),
        ContactPhone("Telefono_Contacto", 15, Event::contactPhone),
        ContactEmail("Email_contacto", 100, Event::contactEmail),
        RequiresRP("Req_Rp", property = Event::requiresRP),
        AssignedRP("Rp_asign", 300, Event::assignedRP),
        RequiresPhotographer("Req_Fot", property = Event::requiresPhotographer),
        AssignedPhotographer("Fotografo_Asignado", 200, Event::assignedPhotographer),
        ObservationsPhotographer("obs_foto", 300, Event::observationsPhotographer),
        RequiresReporter("Req_Rep", property = Event::requiresReporter),
        AssignedReporter("Reportero_Asignado", 200, Event::assignedReporter),
        RequiresDigital("Req_Dig", property = Event::requiresDigital),
        AssignedDigital("Digital_Asignado", 200, Event::assignedDigital),
        ObservationsDigital("Observaciones_Digital", 500, Event::observationsDigital),
        Observations("Observaciones", property = Event::observations),
        PicturesNote("nota_foto", property = Event::picturesNote),
        PicturesURL("Url_fotos", 200, property = Event::picturesURL),
        ReportURL("Url_Reporteo", 250, property = Event::reportURL),
        MapURL("url_mapa", 500, Event::mapURL),
        Timestamp("PRD_TIMEST", property = Event::timestamp)
    }

    override val table = "eventos"

    var id: Int = -1

    var edition: Edition = Edition.Puebla
    var product: Product = Product.Synthesis
    var editionType: EditionType = EditionType.Regular
    var eventType: EventType = EventType.Social
    
    var description:    String = ""
    var pictureType:    String = ""
    var pages:          String = ""
    var applicant:      String = ""
    var eventDuration:  String = ""
    var place:          String = ""
    var contactName:    String = ""
    var contactPhone:   String = ""
    var contactEmail:   String = ""
    var observations:   String = ""
    var picturesNote:   String = ""

    var requiresRP: Boolean = false
    lateinit var assignedRP: String

    var requiresPhotographer: Boolean = false
    var assignedPhotographer: String = ""
    var observationsPhotographer: String = ""

    var requiresReporter: Boolean = false
    var assignedReporter: String = ""

    var requiresDigital: Boolean = false
    var assignedDigital: String = ""
    var observationsDigital: String = ""

    var picturesURL: String = ""
    var reportURL: String = ""
    var mapURL: String = ""

    lateinit var publicationDate: LocalDate
    lateinit var coverageDate: LocalDateTime

    var timestamp: LocalDateTime = LocalDateTime.now()

    val item by lazy { EventItem(this) }

    override suspend fun insert(): Boolean = insert(table, mapValues())

    override suspend fun update(): Boolean = update(table, mapValues(), Column.ID.field, id.toString())

    override suspend fun delete(): Boolean = delete(table, Column.ID.field, id.toString())

    override suspend fun fetch(): Event =
        query("SELECT * FROM $table WHERE ${Column.ID.field} = $id;")?.nextRow?.let {
            setValuesFromRow(it)
        } ?: this

    override fun toString(): String = "$id = ${mapValues()}"

    private fun MySQLRow.getBoolean(column: String) = getStringOrEmpty(column).let { it == "Si" }

    private fun mapValues(): Map<String, String> = Column.values().map {
        val actual = when (val value = it.property.get(this)) {
            is APSEnum<*> -> value.dbValue
            is LocalDateTime -> value.toDBString()
            is LocalDate -> value.toDBString()
            is Boolean -> value.toDBValue()
            else -> value.toString()
        }
        it.field to actual
    }.drop(1).dropLast(1).toMap() // Drop the id and timestamp since they are not used in
    // creation or edition

    private fun setValuesFromRow(row: MySQLRow): Event {
        id = row.getInt(Column.ID.field)
        edition = row.getString(Column.Edition.field).let { edition ->
            Edition.values().first { it.dbValue == edition }
        }
        product = row.getString(Column.Product.field).let { product ->
            Product.values().first { it.dbValue == product }
        }
        editionType = row.getString(Column.EditionType.field).let { type ->
            EditionType.values().first { it.dbValue == type }
        }
        eventType = row.getString(Column.EventType.field).let { type ->
            EventType.values().first { it.dbValue == type }
        }
        description = row.getStringOrEmpty(Column.Description.field)
        pictureType = row.getStringOrEmpty(Column.PictureType.field)
        pages = row.getStringOrEmpty(Column.Pages.field)
        applicant = row.getString(Column.Applicant.field)
        eventDuration = row.getStringOrEmpty(Column.EventDuration.field)
        place = row.getStringOrEmpty(Column.Place.field)
        contactName = row.getStringOrEmpty(Column.ContactName.field)
        contactPhone = row.getStringOrEmpty(Column.ContactPhone.field)
        contactEmail = row.getStringOrEmpty(Column.ContactEmail.field)
        requiresRP = row.getBoolean(Column.RequiresRP.field)
        assignedRP = row.getStringOrEmpty(Column.AssignedRP.field)
        requiresPhotographer = row.getBoolean(Column.RequiresPhotographer.field)
        assignedPhotographer = row.getStringOrEmpty(Column.AssignedPhotographer.field)
        observationsPhotographer = row.getStringOrEmpty(Column.ObservationsPhotographer.field)
        requiresReporter = row.getBoolean(Column.RequiresReporter.field)
        assignedReporter = row.getStringOrEmpty(Column.AssignedReporter.field)
        requiresDigital = row.getBoolean(Column.RequiresDigital.field)
        assignedDigital = row.getStringOrEmpty(Column.AssignedDigital.field)
        observationsDigital = row.getStringOrEmpty(Column.ObservationsDigital.field)
        observations = row.getStringOrEmpty(Column.Observations.field)
        picturesNote = row.getStringOrEmpty(Column.PicturesNote.field)
        picturesURL = row.getStringOrEmpty(Column.PicturesURL.field)
        reportURL = row.getStringOrEmpty(Column.ReportURL.field)
        mapURL = row.getStringOrEmpty(Column.MapURL.field)

        val publicationDateStr = row.getString(Column.PublicationDate.field)
        publicationDate = publicationDateStr.toLocalDate()
        val coverageDateStr = row.getString(Column.CoverageDate.field)
        coverageDate = coverageDateStr.toLocalDateTime()
        val prdTime = row.getString(Column.Timestamp.field)
        timestamp = prdTime.toLocalDateTime()

        return this
    }

}