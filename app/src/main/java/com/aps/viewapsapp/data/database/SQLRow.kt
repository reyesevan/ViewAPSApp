package com.aps.viewapsapp.data.database

abstract class SQLRow: SQL() {

    abstract val table: String

    abstract suspend fun insert(): Boolean

    abstract suspend fun update(): Boolean

    abstract suspend fun delete(): Boolean

    abstract suspend fun fetch(): SQLRow

}