package com.aps.viewapsapp.data.models

import com.BoardiesITSolutions.AndroidMySQLConnector.ResultSet
import com.aps.viewapsapp.data.database.SQLTable

object Events: SQLTable<Event>("eventos") {

    override fun ResultSet?.toList(): List<Event> = this?.let { result ->
        (0 until result.numRows).mapNotNull {
            try { Event(result.nextRow) } catch (e: Exception) { null }
        }
    } ?: emptyList()

    override fun ResultSet?.toObjectOrNull(): Event? =
        if (this != null && numRows > 1) Event(nextRow) else null

}