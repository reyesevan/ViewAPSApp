package com.aps.viewapsapp.data.models

interface APSEnum<T> {

    val id: Int?

    val dbValue: String

}