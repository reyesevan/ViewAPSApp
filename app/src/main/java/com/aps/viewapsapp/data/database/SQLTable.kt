package com.aps.viewapsapp.data.database

import com.BoardiesITSolutions.AndroidMySQLConnector.ResultSet
import com.aps.viewapsapp.data.models.APSEnum
import com.aps.viewapsapp.utils.toDBString
import com.aps.viewapsapp.utils.toDBValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

abstract class SQLTable <R: SQLRow> (val table: String): SQL() {

    val query: Query
        get() = Query()

    protected abstract fun ResultSet?.toList(): List<R>

    protected abstract fun ResultSet?.toObjectOrNull(): R?

    suspend fun getAll(): List<R> = withContext(Dispatchers.Default) {
        select(table).toList()
    }

    suspend fun updateAll(recordsToUpdate: List<R>? = null) {
        (recordsToUpdate ?: getAll()).forEach { it.update() }
    }

    suspend fun deleteAll(recordsToDelete: List<R>) {
        recordsToDelete.forEach { it.delete() }
    }

    inner class Query {

        private val query: String
            get() = StringBuilder("SELECT * FROM $table").run {
                if (conditions.isNotEmpty())
                    append(" WHERE ${conditions.joinToString(" ")}")
                append(orderBy)
                append(limit)
                append(";")
                toString()
            }

        private val conditions: MutableList<String> = mutableListOf()

        private var lastCallWasOperator: Boolean = true

        private var orderBy: String? = null
            get() = if (field == null)
                ""
            else
                " ORDER BY $field ${if (ascending) "ASC" else "DESC"}"

        private var ascending: Boolean = true

        private var limit: String? = null
            get() = if (field == null)
                ""
            else
                " LIMIT $field"

        fun and(): Query {
            lastCallWasOperator = true
            conditions.add("AND")
            return this
        }

        fun or(): Query {
            lastCallWasOperator = true
            conditions.add("OR")
            return this
        }

        fun whereEqualTo(column: String, value: Any) = regularCondition(column, value, "=")

        fun whereNotEqualTo(column: String, value: Any) = regularCondition(column, value, "<>")

        fun whereGreaterThan(column: String, value: Any) = regularCondition(column, value, ">")

        fun whereGreaterOrEqualThan(column: String, value: Any) = regularCondition(column, value, ">=")

        fun whereLesserThan(column: String, value: Any) = regularCondition(column, value, "<")

        fun whereBetween(column: String, date1: LocalDate, date2: LocalDate): Query {
            if (!lastCallWasOperator)
                and()
            conditions.add("$column BETWEEN '$date1' AND '$date2'")
            return this
        }

        fun orderBy(column: String, ascending: Boolean = true) {
            orderBy = column
            this.ascending = ascending
        }

        fun limitTo(amount: Int) {
            limit = amount.toString()
        }

        private fun regularCondition(column: String, value: Any, op: String): Query {
            if (!lastCallWasOperator)
                and()
            lastCallWasOperator = false
            val actual = when (value) {
                is APSEnum<*> -> value.dbValue
                is LocalDateTime -> value.toDBString()
                is LocalDate -> value.toDBString()
                is Boolean -> value.toDBValue()
                else -> value.toString()
            }
            conditions.add("$column $op '$actual'")
            return this
        }

        fun seeQuery(): String = query

        suspend fun find(): List<R> = query(query).toList()

    }

}