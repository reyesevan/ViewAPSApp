package com.aps.viewapsapp.data.database

import android.util.Log
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.InvalidSQLPacketException
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.MySQLConnException
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.MySQLException
import com.BoardiesITSolutions.AndroidMySQLConnector.IConnectionInterface
import com.BoardiesITSolutions.AndroidMySQLConnector.IResultInterface
import com.BoardiesITSolutions.AndroidMySQLConnector.ResultSet
import com.aps.viewapsapp.R
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.scape
import com.aps.viewapsapp.utils.stripAccents
import com.aps.viewapsapp.utils.toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

abstract class SQL {

    protected suspend fun select(table: String, columns: List<String>? = null,
                                 vararg conditions: Pair<String, Any>): ResultSet? {
        val cols = if (columns.isNullOrEmpty()) "*" else columns.joinToString()
        return query("SELECT $cols FROM $table ${conditions.toWhereClause()};")
    }

    protected suspend fun insert(table: String, cols: List<String>, values: List<String>): Boolean =
            insert(table, cols.zip(values).toMap())

    protected suspend fun insert(table: String, values: Map<String, String>): Boolean = execute("""
        |INSERT INTO $table
        |(${values.keys.joinToString()})
        |VALUES (${values.values.joinToString { "'${it.stripAccents().scape()}'" }});
    """.trimIndent().trimMargin())

    protected suspend fun update(table: String, changes: Map<String, String>,
                                 key: String, value: String): Boolean {
        val changesStr = changes.map { (k, v) -> "$k = '${v.stripAccents().scape()}'" }.joinToString()
        return execute("UPDATE $table SET $changesStr WHERE $key = '${value.stripAccents().scape()}';")
    }

    protected suspend fun delete(table: String, key: String, value: String): Boolean
            = execute("DELETE FROM $table WHERE $key = '${value.stripAccents().scape()}';")

    protected suspend fun execute(query: String): Boolean = withContext(Dispatchers.IO) {
        Log.d(TAG, "execute: $query")
        val connection = DataModule.provideDBConnection()
        suspendCoroutine {
            connection?.createStatement()?.execute(query, object: IConnectionInterface {
                override fun actionCompleted() {
                    connection.close()
                    it.resume(true)
                }

                override fun handleIOException(ex: IOException?) {
                    toast(R.string.couldNotConnectToDB)
                }

                override fun handleInvalidSQLPacketException(ex: InvalidSQLPacketException?) = handleException(ex)
                override fun handleMySQLException(ex: MySQLException?) = handleException(ex)
                override fun handleMySQLConnException(ex: MySQLConnException?) = handleException(ex)
                override fun handleException(ex: Exception?) {
                    Log.e(this@SQL.TAG, "Could not execute query $query", ex)
                    it.resume(false)
                }
            }) ?: it.resume(false)
        }
    }

    protected suspend fun query(query: String): ResultSet? = withContext(Dispatchers.IO) {
        Log.d(TAG, "query: $query")
        val connection = DataModule.provideDBConnection()
        Log.d(TAG, "query: connection has been provided (null? ${connection == null})")
        suspendCoroutine {
            connection?.createStatement()?.executeQuery(query, object : IResultInterface {
                override fun executionComplete(resultSet: ResultSet?) {
                    connection.close()
                    it.resume(resultSet)
                }

                override fun handleIOException(ex: IOException?) {
                    toast(R.string.couldNotConnectToDB)
                }

                override fun handleInvalidSQLPacketException(ex: InvalidSQLPacketException?) =
                    handleException(ex)
                override fun handleMySQLException(ex: MySQLException?) = handleException(ex)
                override fun handleMySQLConnException(ex: MySQLConnException?) = handleException(ex)
                override fun handleException(ex: Exception?) {
                    Log.e(this@SQL.TAG, "Could not retrieve results for query '$query'", ex)
                    it.resume(null)
                }
            }) ?: it.resume(null)
        }
    }

    private fun <T, S> Array<out Pair<T, S>>.toWhereClause(sep: String = " AND ", op: String = "=")
            = if (isEmpty()) "" else "WHERE ${joinToString(sep) { (k, v) -> "$k $op '$v'" }}"

}