package com.aps.viewapsapp.data.database

import android.util.Log
import com.BoardiesITSolutions.AndroidMySQLConnector.Connection
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.InvalidSQLPacketException
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.MySQLConnException
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.MySQLException
import com.BoardiesITSolutions.AndroidMySQLConnector.IConnectionInterface
import com.aps.viewapsapp.R
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.properties.Delegates

object DataModule {

    suspend fun provideDBConnection(): Connection? = withContext(Dispatchers.IO) {
        val hostname = ""
        val username = ""
        val password = ""
        val port = 0
        val database = ""

        suspendCoroutine {
            var connection: Connection? = null
            var completion: Boolean? by Delegates.observable(null) { _, _, new ->
                Log.d(TAG, "provideDBConnection: callback result = $new")
                it.resume(if (new == true) connection else null)
            }
            connection = Connection(hostname, username, password, port, database,
                    object: IConnectionInterface {
                        override fun actionCompleted() {
                            Log.d(TAG, "Successfully connected to the database")
                            completion = true
                        }

                        override fun handleIOException(ex: IOException?) {
                            toast(R.string.couldNotConnectToDB)
                        }

                        override fun handleInvalidSQLPacketException(ex: InvalidSQLPacketException?) =
                            handleException(ex)
                        override fun handleMySQLException(ex: MySQLException?) =
                            handleException(ex)
                        override fun handleMySQLConnException(ex: MySQLConnException?) =
                            handleException(ex)
                        override fun handleException(ex: Exception?) {
                            completion = false
                            Log.e(this@DataModule.TAG, "Could not connect to the database", ex)
                        }
                    })
        }
    }

}