package com.aps.viewapsapp.data.models

import androidx.annotation.StringRes
import com.BoardiesITSolutions.AndroidMySQLConnector.MySQLRow
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.database.SQLRow
import com.aps.viewapsapp.user.CurrentUser
import com.aps.viewapsapp.utils.getStringOrEmpty

class User(row: MySQLRow): SQLRow() {

	enum class UserType(@StringRes val id: Int, vararg val ids: String) {
		MANAGER(R.string.admin, "Administrador", "Admin"),
		COMMERCIAL(R.string.commercial, "Comercial"),
		PHOTOGRAPHER(R.string.photographer, "Foto", "Fotografo"),
		REPORTER(R.string.reporter, "Reportero"),
		PUBLIC_RELATIONS(R.string.publicRelations, "Relaciones Publicas", "RP"),
		DIRECTION(R.string.direction, "Direccion"),
		INFORMATION(R.string.information, "Informacion"),
		EDITION(R.string.editor, "Edición", "Edicion"),
		DIGITAL(R.string.digital, "Digital", "Digitla"),
		MARKETING(R.string.marketing, "Mercadotecnia"),
		CUSTOMER_SERVICE(R.string.customerService, "Atencion Clientes"),
		OTHER(-1)
	}

	enum class Privileges(val level: Int, @StringRes val type: Int,
						  val create: Boolean, val update: Boolean, val delete: Boolean) {
		Dummy(0, R.string.dummy, false, false, false),
		Admin(1, R.string.admin, true, true, true),
		Adviser(2, R.string.adviser, true, false, false),
		Photographer(3, R.string.photographer, false, true, false),
		Digital(4, R.string.digital, false, true, false),
		Reporter(5, R.string.reporter, false, true, false),
		Editor(6, R.string.editor, false, false, false),
		PR(7, R.string.publicRelations, true, false, false)
	}

    companion object {
		const val ID 			= "idUsuario"
		const val NAME 			= "Nombre"
		const val LAST_NAME 	= "Apellidos"
		const val EMAIL 		= "Correo"
		const val TYPE_OF_USER 	= "Usuario"
		const val PASSWORD 		= "Clave"
		const val PRIVILEGES 	= "Privilegios"
    }

	override val table: String = "usuario"

	val id: String = row.getString(ID)

	val name: String = row.getStringOrEmpty(NAME)

	val lastName: String = row.getStringOrEmpty(LAST_NAME)

	val wholeName: String = "$name $lastName"

	val email: String = row.getString(EMAIL)

	val privileges: Int = row.getInt(PRIVILEGES)

	val userType: UserType = row.getString(TYPE_OF_USER)?.let { type ->
		try {
			UserType.values().first { type in it.ids }
		} catch (e: NoSuchElementException) { UserType.OTHER}
	} ?: UserType.OTHER

	private val password = row.getString(PASSWORD)

	fun credentialsMatch(password: String): Boolean = password == this.password

	fun encryptAndPersistCredentials(savePassword: Boolean) {
		CurrentUser.email = email
		CurrentUser.privilegesLevel = privileges
		if (savePassword)
			CurrentUser.password = password
	}

	override suspend fun insert(): Boolean {
		TODO("Not yet implemented")
	}

	override suspend fun update(): Boolean {
		TODO("Not yet implemented")
	}

	override suspend fun delete(): Boolean {
		TODO("Not yet implemented")
	}

	override suspend fun fetch(): SQLRow {
		TODO("Not yet implemented")
	}
}
