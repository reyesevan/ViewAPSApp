package com.aps.viewapsapp.events.details

import android.app.Activity
import android.content.Intent
import android.view.View
import com.aps.viewapsapp.App
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.databinding.EventItemBinding
import com.aps.viewapsapp.utils.show
import com.xwray.groupie.viewbinding.BindableItem
import java.io.Serializable
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class EventItem(private val event: Event): BindableItem<EventItemBinding>(), Serializable {

    override fun getLayout(): Int = R.layout.event_item

    override fun initializeViewBinding(view: View): EventItemBinding = EventItemBinding.bind(view)

    override fun bind(viewBinding: EventItemBinding, position: Int) {
        with (viewBinding) {
            val edition = event.edition.dbValue
            val editionType = App.context.getString(event.editionType.id)
            val product = App.context.getString(event.product.id)

            ltvEditionTypeProduct.txvText.text = App.context.getString(R.string.editionTypeProduct, edition, editionType, product)
            ltvEventType.txvText.text = App.context.getString(event.eventType.id)
            ltvDescription.txvText.text = event.description
            txvLocation.txvText.text = event.place
            txvApplicant.txvText.text = event.applicant

            val dFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
            val dtFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

            txvCoverageDate.txvText.text = event.coverageDate.format(dtFormatter)
            txvPublicationDate.txvText.text = event.publicationDate.format(dFormatter)

            val assignedMembers = listOf(event.assignedPhotographer to txvPhotographer,
                event.assignedRP to txvAssignedReporter,
                event.assignedReporter to txvAssignedReporter,
                event.assignedDigital to txvAssignedDigital
            )

            assignedMembers.forEach { (name, textView) ->
                if (name.isNotEmpty()) {
                    textView.show()
                    textView.txvText.text = name
                }
            }

//            if (event.assignedPhotographer.isNotEmpty()) {
//                txvPhotographer.show()
//                txvPhotographer.txvText.text = event.assignedPhotographer
//            }

            root.setOnClickListener {
                val activity = it.context as Activity
                activity.startActivityForResult(Intent(activity, EventActivity::class.java).apply {
                    putExtra(EventActivity.Mode.ARG, EventActivity.Mode.Visualize)
                    putExtra(Event.ARG, event)
                }, EventActivity.REQUEST_CODE)
            }
        }
    }

}