package com.aps.viewapsapp.events.layouts

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.databinding.RetrieveMoreItemBinding
import com.aps.viewapsapp.databinding.SimpleListFragmentBinding
import com.aps.viewapsapp.utils.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class SimpleListFragment: Fragment(), Layout {

    companion object {
        private const val FUTURE_ARG = "IsFutureEvents"

        @JvmStatic fun newInstance(isFutureEvents: Boolean) = SimpleListFragment().apply {
            arguments = bundleOf(FUTURE_ARG to isFutureEvents)
        }
    }

    private val viewModel: EventsViewModel by viewModels()

    private val binding by lazy { SimpleListFragmentBinding.inflate(layoutInflater) }

    private val isFutureEventsHolder: Boolean by lazy { arguments?.getBoolean(FUTURE_ARG)!! }

    private val eventsAdapter = GroupAdapter<GroupieViewHolder>()

    private val retrieveMoreItem by lazy {
        object: BindableItem<RetrieveMoreItemBinding>() {
            override fun bind(viewBinding: RetrieveMoreItemBinding, position: Int) {
                viewBinding.txvRetrieveMore.setOnClickListener {
                    viewModel.retrievePreviousMonths()
                }
            }

            override fun getLayout(): Int = R.layout.retrieve_more_item

            override fun initializeViewBinding(view: View) = RetrieveMoreItemBinding.bind(view)
        }
    }

    private val searchListener = object: SearchView.OnQueryTextListener {
        private val regexOptions = setOf(RegexOption.LITERAL, RegexOption.IGNORE_CASE)
        override fun onQueryTextSubmit(query: String?): Boolean {
            binding.svSearchEvents.hideKeyboard()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            if (newText != null)
                viewModel.showEventsForQuery(Regex(newText, regexOptions))
            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startEventsRetrieval()
    }

    override fun onCreateView(i: LayoutInflater, c: ViewGroup?, b: Bundle?): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with (binding) {
            val layoutManager = LinearLayoutManager(context)
            rvEvents.adapter = eventsAdapter
            rvEvents.layoutManager = layoutManager
            rvEvents.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation).apply {
                ContextCompat.getDrawable(requireContext(), R.drawable.divider_item)?.let { setDrawable(it) }
            })

            if (!isFutureEventsHolder) {
                txvOldestDateRetrieved.show()
                viewModel.oldestDateRetrieved.observe(viewLifecycleOwner) {
                    val date = it.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))
                    txvOldestDateRetrieved.text = getString(R.string.oldestRetrieved, date)
                }
            }

            svSearchEvents.setOnQueryTextListener(searchListener)
            svSearchEvents.setOnClickListener { svSearchEvents.isIconified = false }
        }

        viewModel.loadingState.observe(viewLifecycleOwner) {
            if (it == LoadingState.Complete) {
                viewModel.showAllEvents()
                binding.pbLoadingEvents.hide()
            }
            else
                binding.pbLoadingEvents.show()
        }

        viewModel.shownEvents.observe(viewLifecycleOwner) {
            if (it != null) {
                val state = binding.rvEvents.layoutManager?.onSaveInstanceState()
                eventsAdapter.update(it, true)
                binding.rvEvents.layoutManager?.onRestoreInstanceState(state)

                if (!isFutureEventsHolder)
                    eventsAdapter.add(retrieveMoreItem)
            }
        }
    }

    override fun addEvent(event: Event) {
        viewModel.addEvent(event)
    }

    override fun updateEvent(event: Event) {
        viewModel.updateEvent(event)
    }

    override fun removeEvent(event: Event) {
        Log.d(TAG, "removeEvent: isFutureEvents?: $isFutureEventsHolder")
        viewModel.removeEvent(event)
    }

    override fun updateAll() {
        viewModel.restartState()
        startEventsRetrieval()
    }

    fun dispatchNestedScrollTo(coordinatorLayout: CoordinatorLayout) {
        binding.rvEvents.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                coordinatorLayout.dispatchNestedScroll(dx, dy, 0, 0, intArrayOf(0, 0))
            }
        })
    }

    private fun startEventsRetrieval() {
        if (isFutureEventsHolder)
            viewModel.retrieveFutureEvents()
        else {
            viewModel.retrievePastEvents()
            viewModel.sort = { sortedByDescending { it.coverageDate } }
        }
    }

}