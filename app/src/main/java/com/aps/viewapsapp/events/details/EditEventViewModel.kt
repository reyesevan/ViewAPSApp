package com.aps.viewapsapp.events.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.widgets.DatePickerBottomSheet
import java.time.LocalDate
import java.time.LocalDateTime

class EditEventViewModel: ViewModel() {

    var event: Event? = null

    private val today = LocalDate.now()
            .plusDays(1)
            .atTime(DatePickerBottomSheet.DEFAULT_HOUR, DatePickerBottomSheet.DEFAULT_MINUTE)

    val selectedCoverageDate: MutableLiveData<LocalDateTime> = MutableLiveData(today)
    val selectedPublicationDate: MutableLiveData<LocalDate> = MutableLiveData(today.toLocalDate())

}