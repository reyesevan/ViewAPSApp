package com.aps.viewapsapp.events.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.databinding.EventFormBinding
import com.aps.viewapsapp.user.TextField
import com.aps.viewapsapp.user.Validation
import com.aps.viewapsapp.utils.TAG
import com.aps.viewapsapp.utils.clearErrorOnTextChanged
import com.aps.viewapsapp.utils.showKeyboard
import com.aps.viewapsapp.widgets.DatePickerBottomSheet
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class EditableEventFragment: Fragment(), DatePickerBottomSheet.OnDatePickedListener {

    companion object {
        @JvmStatic
        fun newInstance(event: Event? = null) = EditableEventFragment().apply {
            arguments = bundleOf(Event.ARG to event)
        }
    }

    private val viewModel: EditEventViewModel by viewModels()

    private lateinit var binding: EventFormBinding
    private var parent: ViewGroup? = null
    private var inflatedLayout: View? = null
    private var dataHasBeenSet = false

    private val lengthLimitedFieldsRelation by lazy {
        mapOf(
                Event.Column.Description to binding.etxDescription,
                Event.Column.Pages to binding.etxPages,
                Event.Column.EventDuration to binding.etxEventDuration,
                Event.Column.Place to binding.etxLocation,
                Event.Column.ContactName to binding.etxContactName,
                Event.Column.ContactPhone to binding.etxContactPhone,
                Event.Column.ContactEmail to binding.etxContactEmail,
                Event.Column.AssignedPhotographer to binding.etxAssignedPhotographer,
                Event.Column.ObservationsPhotographer to binding.etxObservationsPhotographer,
                Event.Column.AssignedDigital to binding.etxAssignedDigital,
                Event.Column.ObservationsDigital to binding.etxObservationsDigital,
                Event.Column.AssignedRP to binding.etxAssignedRP,
                Event.Column.AssignedReporter to binding.etxAssignedReporter,
                Event.Column.PicturesURL to binding.etxPicturesURL,
                Event.Column.ReportURL to binding.etxReportURL,
                Event.Column.MapURL to binding.etxMapsURL
        )
    }

    private val enumeratedFieldsRelation by lazy {
        listOf(
                Event.Edition.values() to binding.spnEdition,
                Event.Product.values() to binding.spnProduct,
                Event.EditionType.values() to binding.spnEditionType,
                Event.EventType.values() to binding.spnEventType,
                Event.applicants.toTypedArray() to binding.spnApplicant,
        )
    }

    private val checkableFieldsRelation by lazy {
        listOf(
                Triple(binding.chbRequiresPhotographer, binding.etxAssignedPhotographer, binding.etxObservationsPhotographer),
                Triple(binding.chbRequiresDigital, binding.etxAssignedDigital, binding.etxObservationsDigital),
                Triple(binding.chbRequiresRP, binding.etxAssignedRP, null),
                Triple(binding.chbRequiresReporter, binding.etxAssignedReporter, null)
        )
    }

    private val validatedFieldsRelation by lazy {
        listOf(
                binding.etxDescription.layout to binding.etxDescription.editText,
                binding.tilCoverageDate to binding.etxCoverageDate,
                binding.tilPublicationDate to binding.etxPublicationDate
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.event = arguments?.getSerializable(Event.ARG) as? Event
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflatedLayout == null)
            AsyncLayoutInflater(requireContext()).inflate(R.layout.event_form, container) { view, _, parent ->
                this.parent = container
                this.inflatedLayout = view
                binding = EventFormBinding.bind(view)
                parent?.addView(binding.root)
                onViewCreated(binding.root, savedInstanceState)
            }
        else {
            binding = EventFormBinding.bind(inflatedLayout!!)
            parent?.addView(binding.root)
            onViewCreated(binding.root, savedInstanceState)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lengthLimitedFieldsRelation.forEach { (column, editText) ->
            editText.maxLength = column.length
        }

        enumeratedFieldsRelation.forEach { (values, spinner) ->
            try {
                spinner.adapter = ArrayAdapter(requireContext(), R.layout.spinner_item, values)
                spinner.selection = 0
            } catch (e: Exception) { e.printStackTrace() }
        }

        checkableFieldsRelation.forEach { (checkBox, assigned, observations) ->
            assigned.isEnabled = false
            observations?.isEnabled = false
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                assigned.isEnabled = isChecked
                observations?.isEnabled = isChecked
                if (isChecked && dataHasBeenSet)
                    assigned.editText.showKeyboard()
            }
        }

        validatedFieldsRelation.forEach { (layout, editText) ->
            editText.clearErrorOnTextChanged(layout)
        }

        if (viewModel.event != null)
            syncEventValues(viewModel.event!!, false)
        dataHasBeenSet = true

        binding.etxCoverageDate.setOnFocusChangeListener(this::showDateTimePicker)
        binding.etxCoverageDate.setOnClickListener(this::showDateTimePicker)

        binding.etxPublicationDate.setOnFocusChangeListener(this::showDatePicker)
        binding.etxPublicationDate.setOnClickListener(this::showDatePicker)

        viewModel.selectedCoverageDate.observe(requireActivity()) {
            val dtFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
            binding.etxCoverageDate.setText(it.format(dtFormatter))
        }

        viewModel.selectedPublicationDate.observe(requireActivity()) {
            val dtFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
            binding.etxPublicationDate.setText(it.format(dtFormatter))
        }

        binding.etxLocation.editText.setSelectAllOnFocus(true)
    }

    override fun onDestroyView() {
        parent?.removeView(binding.root)
        super.onDestroyView()
    }

    override fun onDateSelected(date: LocalDate) {
        viewModel.selectedPublicationDate.value = date
    }

    override fun onDateTimeSelected(dateTime: LocalDateTime) {
        viewModel.selectedCoverageDate.value = dateTime
    }

    fun syncEvent(): Event? {
        val receiver = viewModel.event ?: Event()
        val syncedEvent = syncEventValues(receiver, true)
        val descriptionField = TextField(syncedEvent.description, binding.etxDescription.layout, mapOf(
            getString(R.string.nonEmpty) to { (it as String).nonEmpty() }
        ))
        val validationResult = Validation.validateInputLayoutFields(descriptionField)
        return if (validationResult) syncedEvent else null
    }

    private fun showDateTimePicker(v: View, hasFocus: Boolean = true) {
        if (hasFocus)
            DatePickerBottomSheet(parentFragmentManager, this, true)
                    .show(parentFragmentManager, null)
    }

    private fun showDatePicker(v: View, hasFocus: Boolean = true) {
        if (hasFocus)
            DatePickerBottomSheet(parentFragmentManager, this, false)
                    .show(parentFragmentManager, null)
    }

    private fun syncEventValues(event: Event, UItoDB: Boolean): Event {
        val textProperties = binding.run {
            listOf(
                etxDescription to Event::description, etxPictureType to Event::pictureType,
                etxPages to Event::pages, binding.etxEventDuration to Event::eventDuration,
                etxLocation to Event::place, etxContactName to Event::contactName,
                etxContactPhone to Event::contactPhone, etxContactEmail to Event::contactEmail,
                etxAssignedPhotographer to Event::assignedPhotographer,
                etxObservationsPhotographer to Event::observationsPhotographer,
                etxAssignedDigital to Event::assignedDigital, etxObservationsDigital to Event::observationsDigital,
                etxAssignedRP to Event::assignedRP, etxAssignedReporter to Event::assignedReporter,
                etxGeneralObservations to Event::observations, etxPicturesNote to Event::picturesNote,
                etxPicturesURL to Event::picturesURL, etxReportURL to Event::reportURL,
                etxMapsURL to Event::mapURL
            )
        }
        val booleanProperties = binding.run {
            listOf(
                chbRequiresPhotographer to Event::requiresPhotographer, chbRequiresDigital to Event::requiresDigital,
                chbRequiresRP to Event::requiresRP, chbRequiresReporter to Event::requiresReporter
            )
        }

        textProperties.forEach { (field, property) ->
            if (UItoDB)
                property.set(event, field.editText.text.toString())
            else
                field.text = property.get(event)
        }

        booleanProperties.forEach { (checkBox, property) ->
            if (UItoDB)
                property.set(event, checkBox.isChecked)
            else
                checkBox.isChecked = property.get(event)
        }

        if (UItoDB) {
            event.coverageDate = viewModel.selectedCoverageDate.value!!
            event.publicationDate = viewModel.selectedPublicationDate.value!!

            event.edition = binding.spnEdition.selectedItem as Event.Edition
            event.product = binding.spnProduct.selectedItem as Event.Product
            event.editionType = binding.spnEditionType.selectedItem as Event.EditionType
            event.eventType = binding.spnEventType.selectedItem as Event.EventType
            event.applicant = binding.spnApplicant.selectedItem as String
        } else {
            Log.d(TAG, "syncEventValues: ")
            viewModel.selectedCoverageDate.value = event.coverageDate
            viewModel.selectedPublicationDate.value = event.publicationDate

            binding.spnEdition.selection = event.edition.ordinal
            binding.spnProduct.selection = event.product.ordinal
            binding.spnEditionType.selection = event.editionType.ordinal
            binding.spnEventType.selection = event.eventType.ordinal
            binding.spnApplicant.selection = try {
                Event.applicants.indexOfFirst { s -> event.applicant == s }
            } catch (e: Exception) { 0 }
        }

        return event
    }

}