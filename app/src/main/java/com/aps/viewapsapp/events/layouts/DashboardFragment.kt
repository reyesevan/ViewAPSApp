package com.aps.viewapsapp.events.layouts

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.databinding.DashboardFragmentBinding
import com.aps.viewapsapp.utils.*
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView
import com.tistory.zladnrms.roundablelayout.RoundableLayout
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.math.abs

class DashboardFragment: Fragment(), Layout {

    companion object {
        @JvmStatic fun newInstance() = DashboardFragment()
    }

    private val viewModel: DashboardViewModel by viewModels()
    private val binding by lazy { DashboardFragmentBinding.inflate(layoutInflater) }

    private val futureEventsFragment by lazy {
        SimpleListFragment.newInstance(true)
    }

    private val pastEventsFragment by lazy {
        SimpleListFragment.newInstance(false)
    }

    private val closestEventAdapter by lazy {
        GroupAdapter<GroupieViewHolder>()
    }

    private val onOffsetChangedListener by lazy {
        AppBarLayout.OnOffsetChangedListener { layout, verticalOffset ->
            if (abs(verticalOffset) == layout.totalScrollRange) {
                binding.rnlTabsContainer.cornerLeftTop = 0F
                binding.rnlTabsContainer.cornerRightTop = 0F
            } else {
                binding.rnlTabsContainer.cornerLeftTop = 16F.px
                binding.rnlTabsContainer.cornerRightTop = 16F.px
            }
        }
    }

    private val viewPagerAdapter by lazy {
        object: FragmentStateAdapter(requireActivity()) {
            override fun getItemCount(): Int = 2

            override fun createFragment(position: Int): Fragment =
                    if (position == 0) futureEventsFragment else pastEventsFragment
        }
    }

    private val onPageChangeCallback = object: ViewPager2.OnPageChangeCallback() {
        var isFirstCallback = true
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            if (isFirstCallback)
                isFirstCallback = false
            else
                viewModel.toggleSelectedTab()
        }
    }

    override fun onCreateView(i: LayoutInflater, c: ViewGroup?, b: Bundle?) = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.findClosestEvent()
        }

        listOf(futureEventsFragment, pastEventsFragment).forEach { fragment ->
            fragment.lifecycleScope.launchWhenCreated {
                fragment.dispatchNestedScrollTo(binding.root)
            }
        }

        with (binding) {
            appBarLayout.addOnOffsetChangedListener(onOffsetChangedListener)

            rvClosestEvent.layoutManager = LinearLayoutManager(context)
            rvClosestEvent.adapter = closestEventAdapter

            vpTabsPlaceholder.adapter = viewPagerAdapter
            vpTabsPlaceholder.registerOnPageChangeCallback(onPageChangeCallback)

            mcvFutureTab.setOnClickListener {
                if (viewModel.selectedTab.value != Tab.FutureEvents)
                    vpTabsPlaceholder.setCurrentItem(0, true)
            }

            mcvPastTab.setOnClickListener {
                if (viewModel.selectedTab.value != Tab.PastEvents)
                    vpTabsPlaceholder.setCurrentItem(1, true)
            }
        }

        viewModel.closestEvent.observe(viewLifecycleOwner) { item ->
            if (item != null)
                closestEventAdapter.update(listOf(item))
        }

        viewModel.selectedTab.observe(viewLifecycleOwner) {
            if (it == Tab.FutureEvents) {
                changeSelectedTab(binding.mcvFutureTab, binding.mcvPastTab)
                binding.mcvBody.setCornerSizes(0F, 16F, 16F, 16F)
            }
            else {
                changeSelectedTab(binding.mcvPastTab, binding.mcvFutureTab)
                binding.mcvBody.setCornerSizes(16F, 0F, 16F, 16F)
            }
        }

    }

    override fun addEvent(event: Event) {
        if (event.id == -1)
            updateAll()
        else {
            val today = LocalDate.now()
            if (event.coverageDate.isAfter(today.atStartOfDay()) || event.publicationDate.isAfter(today))
                futureEventsFragment.addEvent(event)
            else
                pastEventsFragment.addEvent(event)
        }
    }

    override fun updateEvent(event: Event) {
        val today = LocalDate.now()
        if (event.coverageDate.isAfter(today.atStartOfDay()) || event.publicationDate.isAfter(today))
            futureEventsFragment.updateEvent(event)
        else
            pastEventsFragment.updateEvent(event)
    }

    override fun removeEvent(event: Event) {
        val today = LocalDate.now()
        if (event.coverageDate.isAfter(today.atStartOfDay()) || event.publicationDate.isAfter(today))
            futureEventsFragment.removeEvent(event)
        else
            pastEventsFragment.removeEvent(event)
    }

    override fun updateAll() {
        futureEventsFragment.updateAll()
        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.findClosestEvent()
        }
    }

    private fun changeSelectedTab(selected: MaterialCardView, unselected: MaterialCardView) {
        selected.apply {
            cardElevation = 4F.px
            setCardBackgroundColor(requireActivity().getAttr(R.attr.colorSurface))
            (children.first() as TextView).apply {
                alpha = 1F
                setTextColor(requireActivity().getAttr(R.attr.colorOnSurface))
            }
        }

        unselected.apply {
            cardElevation = 0F
            setCardBackgroundColor(requireActivity().getColor(R.color.translucent))
            (children.first() as TextView).apply {
                alpha = 0.8F
                setTextColor(requireActivity().getAttr(R.attr.colorOnPrimary))
            }
        }
    }
}