package com.aps.viewapsapp.events.details

import com.aps.viewapsapp.data.models.Event

interface OnEventCRUD {

    fun onEventUpdated(successful: Boolean, event: Event?)

    fun onEventCreated(successful: Boolean, event: Event?)

    fun onEventDeleted(successful: Boolean, event: Event?)

}