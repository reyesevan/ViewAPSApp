package com.aps.viewapsapp.events.layouts

import com.aps.viewapsapp.data.models.Event

interface Layout {

    fun addEvent(event: Event)

    fun updateEvent(event: Event)

    fun removeEvent(event: Event)

    fun updateAll()

}