package com.aps.viewapsapp.events.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aps.viewapsapp.data.models.Event

class EventViewModel: ViewModel() {

    var event: Event? = null

    val mode: MutableLiveData<EventActivity.Mode> = MutableLiveData(EventActivity.Mode.Visualize)

}