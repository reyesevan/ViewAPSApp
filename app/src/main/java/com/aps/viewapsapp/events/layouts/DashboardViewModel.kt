package com.aps.viewapsapp.events.layouts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.data.models.Events
import com.aps.viewapsapp.events.details.EventItem
import com.aps.viewapsapp.utils.Tab
import java.time.LocalDateTime

class DashboardViewModel: ViewModel() {

    private val _selectedTab: MutableLiveData<Tab> = MutableLiveData(Tab.FutureEvents)
    val selectedTab: LiveData<Tab> = _selectedTab

    private val _closestEvent: MutableLiveData<Event?> = MutableLiveData()
    val closestEvent: LiveData<EventItem?> = Transformations.map(_closestEvent) { it?.item }

    fun toggleSelectedTab() {
        _selectedTab.value?.let {
            _selectedTab.postValue(if (it == Tab.PastEvents) Tab.FutureEvents else Tab.PastEvents)
        }
    }

    suspend fun findClosestEvent() {
        val futureEvents = Events.query
            .whereGreaterOrEqualThan(Event.Column.CoverageDate.field, LocalDateTime.now()).find()
        _closestEvent.postValue(futureEvents.minByOrNull { it.coverageDate })
    }

}