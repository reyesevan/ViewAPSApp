package com.aps.viewapsapp.events.layouts

import android.util.Log
import androidx.lifecycle.*
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.data.models.Events
import com.aps.viewapsapp.events.details.EventItem
import com.aps.viewapsapp.utils.LoadingState
import com.aps.viewapsapp.utils.TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import kotlin.properties.Delegates

class EventsViewModel: ViewModel(), Layout {

    private var allCachedEvents: Set<Event> by Delegates.observable(emptySet()) { _, _, newValue ->
        _allEvents.postValue(newValue.toList())
    }

    private val _allEvents: MutableLiveData<List<Event>> = MutableLiveData(emptyList())
    val allEvents: LiveData<List<Event>> = _allEvents

    private val _shownEvents: MutableLiveData<List<Event>> = MutableLiveData(emptyList())
    val shownEvents: LiveData<List<EventItem>> =
        Transformations.map(_shownEvents) { events ->
            events?.sort()?.map { it.item }.orEmpty()
        }

    private val _oldestDateRetrieved: MutableLiveData<LocalDate> = MutableLiveData()
    val oldestDateRetrieved: LiveData<LocalDate> = _oldestDateRetrieved

    private val _loadingState: MutableLiveData<LoadingState> = MutableLiveData(LoadingState.Uninitialized)
    val loadingState: LiveData<LoadingState> = _loadingState

    private val defaultMonthsOffset: Long = 1

    var sort: List<Event>.() -> List<Event> = { sortedBy { it.coverageDate } }

    fun retrieveAllEvents() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingState.postValue(LoadingState.Loading)
            val retrievedEvents = Events.getAll()
            allCachedEvents = retrievedEvents.toSet()
            _allEvents.postValue(retrievedEvents)
            _oldestDateRetrieved.postValue(LocalDate.MIN)
            _loadingState.postValue(LoadingState.Complete)
        }
    }

    fun retrieveDefaultEvents() {
        if (_loadingState.value == LoadingState.Uninitialized)
            viewModelScope.launch { initializeEvents() }
        else
            Log.w(TAG, "retrieveDefaultEvents: look out! Trying to call an initializing function after initialization")
    }

    fun retrieveFutureEvents() {
        if (_loadingState.value == LoadingState.Uninitialized)
            viewModelScope.launch { initializeEvents(0, false) }
        else
            Log.w(TAG, "retrieveFutureEvents: look out! Trying to call an initializing function after initialization")
    }

    fun retrievePastEvents() {
        if (_loadingState.value == LoadingState.Uninitialized) {
            _oldestDateRetrieved.value = LocalDate.now().plusDays(1)
            viewModelScope.launch {
                retrieveMoreEvents()
                allCachedEvents = allCachedEvents.filterNot {
                    it.publicationDate.isAfter(LocalDate.now())
                }.toSet()
            }
        }
        else
            Log.w(TAG, "retrievePastEvents: look out! Trying to call an initializing function after initialization")
    }

    fun retrievePreviousMonths() {
        if (_loadingState.value != LoadingState.Uninitialized)
            viewModelScope.launch { retrieveMoreEvents() }
        else
            Log.w(TAG, "retrieveDefaultEvents: look out! Trying to call without initializing first")
    }

    fun showAllEvents() {
        _shownEvents.postValue(allCachedEvents.toList())
    }

    fun showEventsForDate(date: LocalDate) {
        val dateEvents = allCachedEvents.filter {
            it.publicationDate.isEqual(date) || it.coverageDate.isEqual(date.atStartOfDay())
        }
        _shownEvents.postValue(dateEvents)
    }

    fun showEventsForQuery(query: Regex) {
        val dFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
        val dtFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

        val eventsFound = allCachedEvents.filter { event ->
            query in event.eventType.dbValue || query in event.applicant
                    || query in event.assignedRP || query in event.assignedReporter
                    || query in event.assignedDigital || query in event.assignedPhotographer
                    || query in event.publicationDate.format(dFormatter)
                    || query in event.coverageDate.format(dtFormatter)
        }
        _shownEvents.postValue(eventsFound)
    }

    fun restartState() {
        _oldestDateRetrieved.value = null
        _loadingState.value = LoadingState.Uninitialized
        _shownEvents.value = emptyList()
        allCachedEvents = emptySet()
    }

    override fun addEvent(event: Event) {
        _loadingState.value = LoadingState.Loading
        allCachedEvents = allCachedEvents + event
        _loadingState.value = LoadingState.Complete
    }

    override fun updateEvent(event: Event) {
        _loadingState.value = LoadingState.Loading
        val currentEvent = allCachedEvents.firstOrNull { it.id == event.id }
        if (currentEvent != null)
            allCachedEvents = allCachedEvents - currentEvent + event
        _loadingState.value = LoadingState.Complete
    }

    override fun removeEvent(event: Event) {
        _loadingState.value = LoadingState.Loading
        val currentEvent = allCachedEvents.firstOrNull { it.id == event.id }
        if (currentEvent != null)
            allCachedEvents = allCachedEvents - currentEvent
        _loadingState.value = LoadingState.Complete
    }

    override fun updateAll() {
        restartState()
        viewModelScope.launch(Dispatchers.IO) {
            initializeEvents(defaultMonthsOffset, true)
        }
    }

    private suspend fun initializeEvents(monthsOffset: Long = defaultMonthsOffset, atFirstDay: Boolean = true) {
        val initialDate = if (atFirstDay)
            LocalDate.now().minusMonths(monthsOffset).withDayOfMonth(1)
        else
            LocalDate.now().minusMonths(monthsOffset)

        _oldestDateRetrieved.postValue(initialDate)
        _loadingState.postValue(LoadingState.Loading)
        val events = withContext(Dispatchers.IO) {
            Events.query.run {
                whereGreaterThan(Event.Column.PublicationDate.field, initialDate)
                or()
                whereGreaterThan(Event.Column.CoverageDate.field, initialDate)
                find()
            }
        }

        allCachedEvents = events.toSet()
        _loadingState.postValue(LoadingState.Complete)
    }

    private suspend fun retrieveMoreEvents(atFirstDay: Boolean = true) {
        _oldestDateRetrieved.value?.let { currentlyOldestDate ->
            val newOldestDate = if (atFirstDay)
                currentlyOldestDate.minusMonths(defaultMonthsOffset).withDayOfMonth(1)
            else
                currentlyOldestDate.minusMonths(defaultMonthsOffset)

            _oldestDateRetrieved.postValue(newOldestDate)
            _loadingState.postValue(LoadingState.Loading)
            val newEvents = withContext(Dispatchers.IO) {
                Events.query.run {
                    whereBetween(Event.Column.PublicationDate.field, newOldestDate, currentlyOldestDate)
                    or()
                    whereBetween(Event.Column.CoverageDate.field, newOldestDate, currentlyOldestDate)
                    find()
                }
            }

            allCachedEvents = allCachedEvents union newEvents
            _loadingState.postValue(LoadingState.Complete)
        }
    }

}