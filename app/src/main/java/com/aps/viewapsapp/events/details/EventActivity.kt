package com.aps.viewapsapp.events.details

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.data.models.User
import com.aps.viewapsapp.databinding.EventActivityBinding
import com.aps.viewapsapp.user.AuthenticationActivity
import com.aps.viewapsapp.user.CurrentUser
import com.aps.viewapsapp.user.OnAuthenticationListener
import com.aps.viewapsapp.utils.toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate

class EventActivity: AppCompatActivity(), OnEventCRUD, OnAuthenticationListener {

    enum class Mode {
        Visualize, Create,  Edit;

        companion object {
            const val ARG = "Mode"
        }
    }

    companion object {
        const val REQUEST_CODE = 868
        const val EVENT_EDITED = 997
        const val EVENT_CREATED = 998
        const val EVENT_DELETED = 999
        const val REQUIRE_AUTHENTICATION = "RequiresAuthentication"
    }

    private val binding by lazy { EventActivityBinding.inflate(layoutInflater) }

    private val viewModel: EventViewModel by viewModels()

    private val editableEventFragment by lazy {
        EditableEventFragment.newInstance(viewModel.event)
    }
    private val informationEventFragment by lazy {
        InformationEventFragment.newInstance(viewModel.event!!)
    }

    private val startedByDeepLink by lazy {
        intent.getBooleanExtra(REQUIRE_AUTHENTICATION, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        if (intent.hasExtra(Mode.ARG))
            viewModel.mode.value = intent.getSerializableExtra(Mode.ARG) as Mode

        if (viewModel.mode.value != Mode.Create)
            if (intent.hasExtra(Event.ARG))
                viewModel.event = intent.getSerializableExtra(Event.ARG) as Event
            else {
                toast("Event not found")
                finish()
            }

        if (startedByDeepLink)
            lifecycleScope.launch {
                CurrentUser.logIn(this@EventActivity)
            }
        else
            attachFragments()

        with (binding.toolbar) {
            setNavigationOnClickListener {
                onBackPressed()
            }

            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menuItemEdit -> viewModel.mode.value = Mode.Edit
                    R.id.menuItemSave -> {
                        val eventToSave = editableEventFragment.syncEvent()
                        if (eventToSave != null)
                            lifecycleScope.launch(Dispatchers.IO) {
                                if (viewModel.mode.value == Mode.Create)
                                    onEventCreated(eventToSave.insert(), eventToSave)
                                else
                                    onEventUpdated(eventToSave.update(), eventToSave)
                            }
                    }
                    R.id.menuItemCancel ->
                        if (viewModel.mode.value == Mode.Create)
                            finish()
                        else
                            viewModel.mode.postValue(Mode.Visualize)
                    R.id.menuItemDelete -> {
                        setResult(EVENT_DELETED, Intent().putExtra(Event.ARG, viewModel.event))
                        finish()
                    }
                }
                true
            }
        }


        viewModel.mode.observe(this) { mode ->
            with (binding.toolbar) {
                title = when (mode) {
                    Mode.Create -> getString(R.string.creatingEvent)
                    Mode.Edit -> getString(R.string.editingEvent)
                    Mode.Visualize -> viewModel.event?.description
                    null -> ""
                }

                menu.findItem(R.id.menuItemSave).isVisible = mode != Mode.Visualize
                menu.findItem(R.id.menuItemCancel).isVisible = mode != Mode.Visualize
                menu.findItem(R.id.menuItemDelete).isVisible = mode ==
                        Mode.Visualize && CurrentUser.privileges.delete
                menu.findItem(R.id.menuItemEdit).isVisible = viewModel.event?.publicationDate?.let {
                    mode == Mode.Visualize
                            && (it.isAfter(LocalDate.now()) || it.isAfter(LocalDate.now()))
                            && CurrentUser.privileges.update
                } ?: false

                if (mode != Mode.Create)
                    supportFragmentManager.commit {
                        detach(if (mode == Mode.Visualize) editableEventFragment else informationEventFragment)
                        attach(if (mode == Mode.Visualize) informationEventFragment else editableEventFragment)
                    }
            }
        }
    }

    override fun onEventUpdated(successful: Boolean, event: Event?) {
        if (successful) {
            toast(R.string.saveSuccessful)
            viewModel.mode.postValue(Mode.Visualize)
            setResult(EVENT_EDITED, Intent().putExtra(Event.ARG, event))
            informationEventFragment.updateUI(event!!)
        } else
            toast(R.string.saveFailed)
    }

    override fun onEventCreated(successful: Boolean, event: Event?) {
        if (successful) {
            toast(R.string.createSuccessful)
            setResult(EVENT_CREATED, Intent().putExtra(Event.ARG, event))
            finish()
        } else
            toast(R.string.createFailed)
    }

    override fun onEventDeleted(successful: Boolean, event: Event?) = Unit

    override fun onSignInFailed(error: OnAuthenticationListener.Error, cachedCredentials: Boolean) {
        Toast.makeText(this, R.string.needAuthentication, Toast.LENGTH_SHORT).show()
        CurrentUser.logOut(this)
    }

    override fun onSignInSuccessful(user: User, cachedCredentials: Boolean) = attachFragments()

    private fun attachFragments() {
        supportFragmentManager.commit {
            add(R.id.fcvEventFormPlaceholder, editableEventFragment)
            if (viewModel.event != null || viewModel.mode.value != Mode.Create) {
                add(R.id.fcvEventFormPlaceholder, informationEventFragment)
                detach(if (viewModel.mode.value == Mode.Visualize) editableEventFragment else informationEventFragment)
            }
        }
    }
}