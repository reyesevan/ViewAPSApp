package com.aps.viewapsapp.events.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.aps.viewapsapp.App
import com.aps.viewapsapp.R
import com.aps.viewapsapp.data.models.Event
import com.aps.viewapsapp.databinding.EventInformationBinding
import com.aps.viewapsapp.utils.hide
import com.aps.viewapsapp.utils.show
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class InformationEventFragment: Fragment() {

    companion object {
        @JvmStatic fun newInstance(event: Event) = InformationEventFragment().apply {
            arguments = bundleOf(Event.ARG to event)
        }
    }

    private val binding by lazy { EventInformationBinding.inflate(layoutInflater) }

    private val event by lazy { arguments?.getSerializable(Event.ARG) as Event }

    private val textFieldsRelation by lazy {
        binding.run {
            listOf(
                    txvApplicant to Event::applicant,
                    txvDescription to Event::description,
                    txvPictureType to Event::pictureType,
                    txvPages to Event::pages,
                    txvDuration to Event::eventDuration,
                    txvLocation to Event::place,
                    txvContactName to Event::contactName,
                    txvContactPhone to Event::contactPhone,
                    txvContactEmail to Event::contactEmail,
                    txvAssignedPhotographer to Event::assignedPhotographer,
                    txvObservationsPhotographer to Event::observationsPhotographer,
                    txvAssignedDigital to Event::assignedDigital,
                    txvObservationsDigital to Event::observationsDigital,
                    txvAssignedRP to Event::assignedRP,
                    txvAssignedReporter to Event::assignedReporter,
                    txvGeneralObservations to Event::observations,
                    txvPicturesNote to Event::picturesNote,
                    txvPicturesURL to Event::picturesURL,
                    txvReportURL to Event::reportURL,
                    txvMapsURL to Event::mapURL
            )
        }
    }

    private val hideableFieldsRelation by lazy {
        binding.run {
            listOf(
                    groupPhotographer to Event::requiresPhotographer,
                    groupDigital to Event::requiresDigital,
                    groupRP to Event::requiresRP,
                    groupReporter to Event::requiresReporter
            )
        }
    }

    override fun onCreateView(i: LayoutInflater, c: ViewGroup?, b: Bundle?): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI(event)
    }

    fun updateUI(event: Event) = with (binding) {
        textFieldsRelation.forEach { (textView, property) ->
            textView.txvText.text = property.get(event)
        }

        hideableFieldsRelation.forEach { (group, property) ->
            if (property.get(event).not())
                group.hide()
        }

        val edition = event.edition.dbValue
        val editionType = getString(event.editionType.id)
        val product = getString(event.product.id)

        txvEditionTypeProduct.txvText.text =
                getString(R.string.editionTypeProduct, edition, editionType, product)
        txvEventType.txvText.text = getString(event.eventType.id)

        val dFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
        val dtFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

        txvCoverageDate.txvText.text = event.coverageDate.format(dtFormatter)
        txvPublicationDate.txvText.text = event.publicationDate.format(dFormatter)
    }
}