package com.aps.viewapsapp.utils

enum class Tab { PastEvents, FutureEvents}

enum class LoadingState { Uninitialized, Loading, Complete }