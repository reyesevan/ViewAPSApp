package com.aps.viewapsapp.utils

import android.graphics.drawable.Drawable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import com.aps.viewapsapp.App
import com.google.android.material.card.MaterialCardView
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

fun View.hide() { visibility = View.GONE }

fun View.show() { visibility = View.VISIBLE }

fun View.enable() { isEnabled = true }

fun View.disable() { isEnabled = false }

fun View.showKeyboard() {
    requestFocus()
    App.inputMethodManager?.showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

fun View.hideKeyboard() {
    App.inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
}

fun MaterialCardView.setCornerSizes(topLeft: Float = 0F,
                                    topRight: Float = 0F,
                                    bottomLeft: Float = 0F,
                                    bottomRight: Float = 0F) {
    shapeAppearanceModel = ShapeAppearanceModel.builder().apply {
        setTopLeftCornerSize(topLeft.px)
        setTopRightCornerSize(topRight.px)
        setBottomLeftCornerSize(bottomLeft.px)
        setBottomRightCornerSize(bottomRight.px)
    }.build()
}

fun TextInputEditText.clearErrorOnTextChanged(layout: TextInputLayout) {
    val listener = { _: Any? -> layout.error = null }
    setOnClickListener(listener)
    doAfterTextChanged(listener)
}