package com.aps.viewapsapp.utils

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.aps.viewapsapp.App

val currentThread: String
    get() = Thread.currentThread().name

fun runOnMainThread(action: () -> Unit) {
    Handler(Looper.getMainLooper()).post(action)
}

fun toast(s: String, block: Toast.() -> Unit = {}) = runOnMainThread {
    Toast.makeText(App.context, s, Toast.LENGTH_SHORT).apply(block).show()
}

fun toast(res: Int, block: Toast.() -> Unit = {}) = runOnMainThread {
    Toast.makeText(App.context, res, Toast.LENGTH_SHORT).apply(block).show()
}
