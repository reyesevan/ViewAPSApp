@file:JvmName("Constants")
package com.aps.viewapsapp.utils

const val CHANNEL_ID = "ViewAPS.NewEventsNotification"

const val CHANNEL_NAME = "Notifications"

const val CHANNEL_DESCRIPTION = "Shows notifications when new events have been registered"
