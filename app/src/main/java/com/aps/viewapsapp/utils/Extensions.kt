package com.aps.viewapsapp.utils

import android.content.Context
import android.util.DisplayMetrics
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import com.BoardiesITSolutions.AndroidMySQLConnector.Exceptions.SQLColumnNotFoundException
import com.BoardiesITSolutions.AndroidMySQLConnector.MySQLRow
import com.aps.viewapsapp.App
import com.donkey.dolly.Dolly
import com.donkey.dolly.Type
import java.text.Normalizer
import java.time.*
import java.time.format.DateTimeFormatter

private val dateTimeFormatter: DateTimeFormatter =
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

val Any.TAG: String?
        get() = this::class.simpleName

val Float.px
        get() = this * (App.context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

val Float.dp: Float
        get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, App.context.resources.displayMetrics)

fun MySQLRow.getStringOrNull(column: String): String? =
        try { getString(column) } catch (e: SQLColumnNotFoundException) { null }

fun MySQLRow.getStringOrEmpty(column: String): String =
        try { getString(column) ?: "" } catch (e: SQLColumnNotFoundException) { "" }

@ColorInt
fun Context.getAttr(@AttrRes attrColor: Int, typedValue: TypedValue = TypedValue()): Int {
        theme.resolveAttribute(attrColor, typedValue, true)
        return typedValue.data
}

//fun Dolly.encrypt(key: String, value: String): Unit = putString(key, value, Type.ENCRYPT)
//fun Dolly.decryptString(key: String): String = getString(key, Type.ENCRYPT)

//fun Dolly.encrypt(@StringRes key: Int, value: Int)
//        = putInt(App.context.getString(key), value, Type.ENCRYPT)

fun Dolly.encrypt(@StringRes key: Int, value: String?) =
        if (value != null)
            putString(App.context.getString(key), value, Type.ENCRYPT)
        else
            remove(App.context.getString(key), Type.ENCRYPT)

fun Dolly.encrypt(@StringRes key: Int, value: Int?) =
        if (value != null)
            putInt(App.context.getString(key), value, Type.ENCRYPT)
        else
            remove(App.context.getString(key), Type.ENCRYPT)

fun Dolly.decryptString(@StringRes key: Int): String? =
        getString(App.context.getString(key), null, Type.ENCRYPT)

fun Dolly.decryptInt(@StringRes key: Int): Int? =
        getInt(App.context.getString(key), Int.MIN_VALUE, Type.ENCRYPT).let {
            if (it != Int.MIN_VALUE) it else null
        }

fun Long.toDateTime(id: String = "UTC"): LocalDateTime =
        LocalDateTime.ofInstant(Instant.ofEpochMilli(this), ZoneId.of("UTC"))

fun String.toLocalDate(): LocalDate = LocalDate.parse(this)

fun String.toLocalDateTime(): LocalDateTime = LocalDateTime.parse(this, dateTimeFormatter)

fun LocalDate.toDBString() = toString()

fun LocalDateTime.toDBString(): String = format(dateTimeFormatter)

fun Boolean.toDBValue(): String = if (this) "Si" else "No"

fun LocalDate.min(other: LocalDate) = if (isBefore(other)) this else other

fun CharSequence.stripAccents(regex: Regex = "\\p{InCombiningDiacriticalMarks}+".toRegex()): String
        = regex.replace(Normalizer.normalize(this, Normalizer.Form.NFD), "")

fun String.scape(char: String = "'"): String = replace(char, "\\$char")