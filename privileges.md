# Privileges

## Event faculties

| Type             | Level | Read | Create | Update | Delete |
| ---------------- | ----- | ---- | ------ | ------ | ------ |
| Admin            | 1     | ✓    | ✓      | ✓      | ✓      |
| Adviser          | 2     | ✓    | ✓      | ✗      | ✗      |
| Photographer     | 3     | ✓    | ✗      | ✓      | ✗      |
| Digital          | 4     | ✓    | ✗      | ✓      | ✗      |
| Reporter         | 5     | ✓    | ✗      | ✓      | ✗      |
| Editor           | 6     | ✓    | ✗      | ✗      | ✗      |
| Public relations | 7     | ✓    | ✓      | ✗      | ✗      |